const functions = require("./index");
const context = require("../testing/context");
const { test } = require("@jest/globals");

test("Http trigger", async () => {
  const request = {
    query: { name: "schuc" },
  };
  var iterations = 100000;
  console.time("FUNCTION #1");
  for (var i = 0; i < iterations; i++) {
    await functions(context, request);
  }
  console.timeEnd("FUNCTION #1");
  expect(context.res.body).toContain("W");
  expect(context.res.body).toEqual("Welcome, schuc");
  //expect(context.log.mock.calls.length).toBe(200);
});

